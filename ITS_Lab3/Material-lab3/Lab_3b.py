"""Question 2 and 3
Compute the CDF of histogram from Question 1 and normalize it
using histogram equalized.

Note: Answer to Question 2 is also shown in Lab_3.py
"""
import cv2
import numpy as np
from matplotlib import pyplot as plt

if __name__ == '__main__':
    image = cv2.imread('nature.jpg')  # read the image

    """do the gray scales conversion"""
    image_gray= cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    """ Normalizing the CDF using histogram equalized function """
    equalized = cv2.equalizeHist(image_gray)
    cv2.imshow('Equalized Image',equalized)
    hist = cv2.calcHist([equalized],[0],None,[256],[0,256])
    """Normalizing the CDF using cumsum()"""
    norm = np.cumsum(hist) / np.sum(hist)
    plt.title('Hist of color pictures')
    plt.plot(hist)
    plt.show()


    while True:
        k = cv2.waitKey(1) & 0xFF
        if k == 27: break
    cv2.destroyALLWindowas()