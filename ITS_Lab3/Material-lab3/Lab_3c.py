"""
Question 4: Function to extract histogram of red, green and blue
pixels from the original image
References:
http://www.bogotobogo.com/python/OpenCV_Python/python_opencv3_image_histogram_calcHist.php

"""
import cv2
from matplotlib import pyplot as plt


if __name__ == '__main__':
    """read the image """
    image = cv2.imread('nature.jpg')
    """Define the blue, green and red colors"""
    color = ('b','g','r')
    """Loop over the  color and store them in a counter variable i """
    for i, col in enumerate(color):
        hist = cv2.calcHist([image],[i],None,[256],[0,256])
        plt.plot(hist,color = col)
        plt.xlim([0,256])
    plt.title('Histogram for color scale pictures')
    plt.show()



    while True:
        k=cv2.waitKey(1) & 0xFF
        if k== 27: break
    cv2.destroyALLWindowas()