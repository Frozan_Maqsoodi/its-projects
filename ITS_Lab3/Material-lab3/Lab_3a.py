"""
Question 1:
Create a program that reads the image
(nature.jpg) in gray scales and extract the histogram of that image

"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

if __name__ == '__main__':
    """read the image """
    image = cv2.imread('nature.jpg')

    """do the gray scales conversion """
    image_gray= cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cv2.imshow('image',image_gray)

    hist = cv2.calcHist([image_gray], [0], None, [256], [0, 256])
    plt.hist(image_gray.ravel(), 256, [0, 256])
    """Normalizing the CDF using cumsum()"""
    norm = np.cumsum(hist) / np.sum(hist)
    plt.plot(norm)
    plt.plot(hist)
    plt.show()


    while True:
        k = cv2.waitKey(1) & 0xFF
        if k == 27: break
    cv2.destroyALLWindowas()