"""
Understanding the image structure
"""

import cv2

""" Read and display the image """
img = cv2.imread('scale.jpg')

"""Printing matrix value of gray image """
grey_img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
print (grey_img)

cv2.namedWindow('Image',cv2.WINDOW_NORMAL)
cv2.imshow('Image',grey_img)
k = cv2.waitKey(0)
if k == ord('e'):
    cv2.destroyAllWindows()
