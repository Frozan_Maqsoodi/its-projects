"""
Approach B:
Exercise 3 and 4
Are the pictures identical?
No,the pictures are not identical. The resulted difference between the two
pictures is shown in the result.jpg, which clearly shows that it is different
from Lena-A.jpg and Lena-B.jpg

References: https://github.com/ibrahimokdadov/openCV3_tutorials/blob/master/OpenCV3_Tutorials_9_Comparing_Images_and_Displaying_Difference.py

"""

import cv2
import numpy as np

image1 = cv2.imread("Lena-A.jpg")
image2 = cv2.imread("Lena-B.jpg")

difference = cv2.subtract(image1, image2)

result = np.any(difference)

if result is True:
    print "The images are same"
else:
    cv2.imwrite("result.jpg",difference)
    print " The images are different "
