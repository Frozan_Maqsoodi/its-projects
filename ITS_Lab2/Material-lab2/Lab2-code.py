"""
Question 1 and 2: Read image and print matrix value of gray scales of image
"""
import cv2

def inverse(imagem, name):
    imagem = (255 - imagem)
    cv2.imshow("Gray Lena",imagem)
    k = cv2.waitKey(0)
    if k == ord('e'):
        cv2.destroyAllWindows()

if __name__ == '__main__':
    """Read the image """
    image = cv2.imread('Lena.jpg')

    """ Transform image color to gray scales """
    gs_imagem = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    """ Calling the inverse() to print gray scales"""
    inverse(gs_imagem, image)

