"""
Question 5:
Detecting objects using color space
"""
import cv2
import numpy as np

if __name__ == '__main__':
    image = cv2.imread(r'YywIS.jpg')

""" define range of blue color in HSV """
lower_blue = np.array([110,50,50])
upper_blue = np.array([130,255,255])

""" define range of green color in HSV """
lower_green = np.array([45,100,50])
upper_green = np.array([75,255,255])

"""define range of red color in HSV """
lower_red = np.array([170, 50, 50])
upper_red = np.array([180, 255, 255])

""" do the color conversion """
hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)


""" Threshold the HSV image to get three colors """
blue_mask = cv2.inRange(hsv, lower_blue, upper_blue)
green_mask = cv2.inRange(hsv,lower_green,upper_green)
red_mask = cv2.inRange(hsv,lower_red,upper_red)

""" save the images """
cv2.imwrite('HSV.jpg',hsv)
cv2.imwrite('blue_mask.jpg',blue_mask)
cv2.imwrite('green_mask.jpg',green_mask)
cv2.imwrite('red_mask.jpg',red_mask)

