"""
Approach A:
Question 3 and 4: Answer if the two pictures are identical
No, the pictures are not identical.
The absdiff() function finds the difference between the images and with the help of
print command we can see the matrix results of the two pictures (Lena-A.jpg and Lena-B.jpg).
The pictures have different matrix values and the difference is shown in the third matrix.
"""

import cv2

""" Finding the difference between two images using absdiff() """
def diff(img,img1):
    difference = cv2.absdiff(img,img1)
    print "\n The matrix of image 1 \n", img
    print "\n The matrix of image 2 \n", img1
    print "\n The matrix of the difference between the two images \n",difference
    cv2.imwrite('Diff.jpg',difference)

img_1 = cv2.imread('Lena-A.jpg')
img_2 = cv2.imread('Lena-B.jpg')
diff(img_1,img_2)

