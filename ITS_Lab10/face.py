"""
I tried with scale factor = 1.03 which gave me more false-postives, with the scale
factor = 1.01 it gave less false positive but 6 faces were not detected.

Reference:
https://stackoverflow.com/questions/36242860/attribute-error-while-using-opencv-for-face-recognition
"""
import cv2
import matplotlib.pyplot as plt

# Get user supplied values
imagePath = "ubuntu_team.jpg"
cascPath = "haarcascade_frontalface.xml"

# Create the haar cascade
faceCascade = cv2.CascadeClassifier(cascPath)

# Create the haar cascade
faceCascade = cv2.CascadeClassifier(cascPath)

# Read the image
image = cv2.imread(imagePath)


plt.imshow(image)
plt.show()

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Detect faces in the image
faces = faceCascade.detectMultiScale(
   gray,
   scaleFactor=1.03,
   minNeighbors=3,
   minSize=(15, 15),
    maxSize= (25,25),
   flags = cv2.CASCADE_SCALE_IMAGE #flags = cv2.cv.CV_HAAR_SCALE_IMAGE
)

print("Found {0} faces!".format(len(faces)))

# Draw a rectangle around the faces
for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

plt.imshow(image)
plt.show()