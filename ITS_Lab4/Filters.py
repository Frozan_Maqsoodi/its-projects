import cv2
import numpy as np
import matplotlib.image as mpimg
from matplotlib import pyplot as plt

img = cv2.imread('Lena.jpg')
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

"""Linear"""
kernel = np.ones((3,3),np.float32) * (0)
kernel [1,2] = 1
print kernel

"""Sharpen"""
# kernel = np.zeros((3, 3), np.float32)
# kernel [1,1] = 2
# print kernel

"""Sobel"""
# kernel = np.zeros((3, 3), np.float32)
# kernel [0,0] = 1
# kernel [0,1] = 0
# kernel [0,2] = -1
# kernel [1,0] = 2
# kernel [1,1] = 0
# kernel [1,2] = -2
# kernel [2,0] = 1
# kernel [2,1] = 0
# kernel [2,2] = -1
# print kernel

"""Gaussian"""
# kernel = np.zeros((3, 3), np.float32)
# kernel [0,0] = 1
# kernel [0,1] = 2
# kernel [0,2] = 1
# kernel [1,0] = 2
# kernel [1,1] = 4
# kernel [1,2] = 2
# kernel [2,0] = 1
# kernel [2,1] = 2
# kernel [2,2] = 1
#
# kernel /= 16
# print kernel

dst = cv2.filter2D(gray,-1,kernel)

cv2.imshow("win",dst)
if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
