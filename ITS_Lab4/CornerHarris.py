"""Feature 2D extraction (Corner Harris)
References: Harris corner detection Opencv documentation from web
"""

import cv2
import numpy as np
"""Reading the image and converting it into grayscale"""
img = cv2.imread('Lena.jpg')
gray1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

gray1 = np.float32(gray1)
"""Detecting the edges using cornerHarris builtin function"""
dst = cv2.cornerHarris(gray1, 2, 3, 0.04)

"""Dilating corner to be visible"""
dst = cv2.dilate(dst, None)

"""Setting threshold for optimal value"""
img[dst > 0.01*dst.max()] = [0, 0, 255]

"""Display the final image with corners detected"""
cv2.imshow('dst', img)

if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()