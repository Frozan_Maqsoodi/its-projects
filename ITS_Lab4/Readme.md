ITS Lab 4

Part 1: All the filters are applied on the image 'Lena.jpg' in Filters.py. The filters' result can be checked by running one filter at a time.
As as example in the file, Filters.py, you will only see the result of Linear filter. All other filters have been commented out. To check the remaining filters,
comment the linear filter and uncomment any other filter in the file.

Part2: Corner Harris
Just run the fule CornerHarris.py