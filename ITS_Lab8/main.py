""""Motion tracker by Lukas Kanade"""

import cv2
import numpy as np

def linear_regression(X, Y):
    x_ = [i for i in range(len(X))]  # list of time periods 0..n

    fit = np.polyfit(x_, X, 1)
    fit_fn_for_X = np.poly1d(fit)  # fit_fn is now a function which takes in x and returns an estimate for y
    next_x = fit_fn_for_X(len(X))

    fit = np.polyfit(x_, Y, 1)
    fit_fn_for_Y = np.poly1d(fit)
    next_y = fit_fn_for_Y(len(Y))

    return next_x, next_y

if __name__ == "__main__":
    # Initialisation
    inputVideo = "output.avi"
    draw_contours, draw_good_feature_tracks, draw_prediction = True, True, True
    color = np.random.randint(0, 255, (100, 3))  # Create some random colors

    # params for ShiTomasi corner detection
    feature_params = dict(maxCorners=100, qualityLevel=0.3, minDistance=7, blockSize=7, useHarrisDetector=False)
    # params for lucas kanade optical flow
    lk_params = dict(winSize=(15, 15), maxLevel=2, criteria=(cv2.TERM_CRITERIA_COUNT | cv2.TERM_CRITERIA_EPS, 30, 0.01))

    # Load video
    cap = cv2.VideoCapture(inputVideo)

    # Take first frame and find good features in it
    ret, old_frame = cap.read()

    old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
    p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)

    while True:
        ret, frame = cap.read()
        if ret is False or (cv2.waitKey(30) & 0xff) == 27: break  # Exit if the video ended

        frame = cv2.GaussianBlur(frame, (11,11), 1)
        new_gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Create a mask image for drawing purposes and calculate optical flow
        mask = np.zeros_like(old_frame)
        p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, new_gray_frame, p0, None, **lk_params)
        good_old, good_new = p0[st == 1], p1[st == 1]  # Select good points

        if draw_good_feature_tracks:
            for i, (new, old) in enumerate(zip(good_new, good_old)):  # draw the tracks
                a, b = new.ravel()  # tmp new value
                c, d = old.ravel()  # tmp old value
                cv2.line(mask, (a, b), (c, d), color[i].tolist(), 1)
                # cv2.circle(frame, (a, b), 5, color[i].tolist(), -1)

        if draw_contours:
            ret, thresh = cv2.threshold(new_gray_frame, 50, 255, 0)
            # contours, hierarchy = cv2.findContours(thresh, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
            image, contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            for cnt in contours:
                x, y, w, h = cv2.boundingRect(cnt)
                cv2.rectangle(mask, (x, y), (x + w, y + h), (255, 0, 0), 2)

            # cv2.drawContours(mask, contours, -1, (0, 255, 0), 1)  # draw all contours in blue color

        # Prediction
        if draw_prediction:
            for i, (new, old) in enumerate(zip(good_new, good_old)):  # draw the tracks
                (a, b), (c, d) = new.ravel(), old.ravel()  # fetch coords of prev. and new. features

                next_x, next_y = linear_regression([a, c], [b, d])
                cv2.line(mask, (a, b), (int(next_x), int(next_y)), color[i].tolist(), 2)

        # Apply the mask to image
        img = cv2.add(frame, mask)
        cv2.imshow('frame', img)

        # Update the previous frame and previous points
        old_gray = new_gray_frame.copy()
        p0 = good_new.reshape(-1, 1, 2)

    cv2.destroyAllWindows()
    cap.release()
