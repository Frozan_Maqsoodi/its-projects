## A
### 1 Examine diff.py
### 2 Run diff.py:
The eagle from the images eagle-1.jpg and eagle-2.jpg is
extracted, which extracts absolute color difference.
The result is shown in the output file "eagle-diff.jpg"

### 3 Changing images from jpg to bmp
Here again the difference in the pictures are calculated and the absolute color difference is calculated.
The result is shown in "traffic-diff.bmp"

## B
### Steps 1 to 4 are implemented in the file setbg.py
### 5 Changing the image files.
The eagle from the images is extracted, the results are saved in
eaglefg.bmp

### 6 Changing images from jpg to bmp
The foreground is extracted from the images and output results are saved in
traffic_fg.jpg