import cv2
import numpy as np

"""Task 1: Defining absdiff function"""
def absdiff (im1, im2):
    if (im1.shape != im2.shape):
        print('Image mismatch')
        return 0
    else:
        height, width, dummy = im1.shape
        diff = cv2.absdiff(im1, im2)
        a = cv2.split(diff)
        sum = np.zeros((height, width), dtype=np.uint8)
        for i in (1, 2, 3):
            ch = a[i - 1]
            cv2.addWeighted(ch, 1.0 / i, sum, float(i - 1) / i, gamma=0.0, dst=sum)
        return sum

"""Task 2 and 3: Defining setBackground function with the arguments
image: an image
diff: difference computed by absdiff function
threshold: threshold value
"""
def setBackground(image, diff, threshold):
    result, mask = cv2.threshold(diff, threshold, 1, cv2.THRESH_BINARY)
    fg = cv2.bitwise_and(image, image, mask = mask)
    return fg

"""Assigning file names"""
filename1 = "traffic-1.bmp"
filename2 = "traffic-2.bmp"

"""Loading the images"""
image1 = cv2.imread(filename1)
image2 = cv2.imread(filename2)

"""Compute color difference and remove background"""
diff = absdiff(image1, image2)

"""Compute color difference and background"""
x = setBackground(image2,diff,10)

"""Save the results in the output files"""
cv2.imwrite('fg_diff.jpg', x)
cv2.imwrite('traffic_diff.bmp',diff)

