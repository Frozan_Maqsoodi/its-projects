"""
Lab7: Background extraction technique to a video frame
References:
http://answers.opencv.org/question/62029/extract-a-frame-every-second-in-python/
https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html

"""
import cv2
import numpy as np

"""
This function takes two images as inputs and returns the absolute color 
difference between the images. It invokes cv2.absDiff to compute the absolute difference 
between image1 and image2. With color images, 3 sets of absolute differences are computed, one for each channel. 
The for loop selects one channel at a time and computes incremental average of the three channels
"""
def absdiff(im1, im2):
    if im1.shape != im2.shape:
        print('Image mismatch')
        return 0
    else:
        height, width, dummy = im1.shape
        diff = cv2.absdiff(im1, im2)
        a = cv2.split(diff)
        sum = np.zeros((height, width), dtype=np.uint8)
        for i in (1, 2, 3):
            ch = a[i - 1]
            cv2.addWeighted(ch, 1.0 / i, sum, float(i - 1) / i, gamma=0.0, dst=sum)
        return sum

"""Defining setBackground function with the arguments
image: an image
diff: difference computed by absdiff function
threshold: threshold value
"""
def setBackground(image, diff, threshold):
    result, mask = cv2.threshold(diff, threshold, 1, cv2.THRESH_BINARY)
    fg = cv2.bitwise_and(image, image, mask=mask)
    return fg

"""Removing background pixels from a video
The function we use here to find Running Average is cv2.addWeighted(). 
For example, if we are watching a video, we keep feeding each frame to this function, 
and the function keep finding the averages of all frames fed to it as per the relation below:
"""
def average(video, sec):
    capture = cv2.VideoCapture(video)
    fps = capture.get(cv2.CAP_PROP_FPS)
    count = 1
    output = None
    while capture.isOpened():
        ret, frame = capture.read()
        if not ret:
            break
        if count >= fps * sec and sec != 0:
            break
        if count == 1:
            output = frame
        else:
            output = cv2.addWeighted(output, count / (count + 1.0), frame, 1.0 / (count + 1.0), 0.0)
        count += 1
    capture.release()

    return output


"""Load video and calculate the average"""
video = 'traffic.avi'

"""Passing average over all images and write the output in a jpg file"""
avg = average(video, 0)
cv2.imwrite("All_Avg.jpg",avg)

"""Define the codec and create VideoWriter object"""
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))

videoAgain = cv2.VideoCapture(video)

while videoAgain.isOpened():
    ret, frame = videoAgain.read()
    if ret == True:
        diff = absdiff(avg,frame)
        """Calculate difference with threshold set to 50"""
        new_frame = setBackground(frame, diff, 50)
        out.write(new_frame)
    else:
        videoAgain.release()
        out.release()
        break

"""Release everything if job is finished"""
cv2.destroyAllWindows()


