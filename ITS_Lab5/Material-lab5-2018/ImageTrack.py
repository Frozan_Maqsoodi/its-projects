from operator import lt

import cv2
import numpy as np

cam = 'motion0025.jpg'
_, img = cam.read()
oldg = cv2.cvtColor(img, cv2.cv.CV_BGR2GRAY)

lk_params = dict(winSize=(10, 10),
                 maxLevel=5,
                 criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

feature_params = dict(maxCorners=3000,
                      qualityLevel=0.5,
                      minDistance=3,
                      blockSize=3)

_, img = cam.read()
img1 = img[bb[0]:bb[2],bb[1]:bb[3]] # crop the bounding box area
g = cv2.cvtColor(img1, cv2.cv.CV_BGR2GRAY) # get grayscale image
pt = cv2.goodFeaturesToTrack(g, **feature_params)
# pt is for cropped image. add x, y in each point.
for i in xrange(len(pt)):
    pt[i][0][0] = pt[i][0][0]+bb[0]
    pt[i][0][1] = pt[i][0][1]+bb[1]

p0 = np.float32(pt).reshape(-1, 1, 2)
newg = cv2.cvtColor(img, cv2.cv.CV_BGR2GRAY)
p0 = np.float32(pt).reshape(-1, 1, 2)

# For Forward-Backward Error method
# using calcOpticalFlowPyrLK on both image frames
# with corresponding tracking points

p1, st, err = cv2.calcOpticalFlowPyrLK(oldg, newg, p0,
                                       None, **lk_params)
p0r, st, err = cv2.calcOpticalFlowPyrLK(newg, oldg, p1,
                                        None, **lk_params)
d = abs(p0 - p0r).reshape(-1, 2).max(-1)
good = d & lt;
1
new_pts = []
for pts, val in np.itertools.izip(p1, good):
    if val:
        # points using forward-backward error
        new_pts.append([pts[0][0], pts[0][1]])