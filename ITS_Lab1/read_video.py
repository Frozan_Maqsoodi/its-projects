import numpy as np
import cv2

cap = cv2.VideoCapture('video1.avi')

# while(cap.isOpened()):
#     ret, frame = cap.read()
#
#     cv2.imshow('frame',frame)
#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break
#
# cap.release()
# cv2.destroyAllWindows()

# Capturing the video in grey
while (True):
    ret, frame = cap.read()
    grey = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame',grey)
    if cv2.waitKey(1)& 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()