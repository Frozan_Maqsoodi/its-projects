import numpy as np
import cv2

cap = cv2.VideoCapture(0)
while(True):
	# Capture frame-by-frame
	ret,frame=cap.read()

	#show the frame
	cv2.imshow('frame',frame)
    

# When everything done, release the capture
cap.release()
cv2.waitKey(0)
cv2.destroyAllWindows()